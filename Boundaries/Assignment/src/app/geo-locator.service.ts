import { Injectable } from '@angular/core';
import { Geometry } from './classes/geometry';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GeoLocatorService {

  private _url:string = "https://api.opencagedata.com/geocode/v1/json?q=";
  private _location:string;
  private locationDetails;
  private geometrydetails = new Geometry;
  private _key:string = "ae1b75214119412a842be517c493fea3";

  constructor(private http: HttpClient) { }

  setLocation(location:string){
    this._location = location;
  }

  getresponse():Geometry{
      this.http.get(this._url+this._location+'&key='+this._key)
        .subscribe(res=>{
          this.locationDetails=res;
          this.geometrydetails.latitude = this.locationDetails.results[0].geometry.lat;
          this.geometrydetails.longitude = this.locationDetails.results[0].geometry.lng;
        });
        return (this.geometrydetails)
      }
}
