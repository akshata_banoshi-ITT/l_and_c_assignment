import { Component } from '@angular/core';
import { Geometry } from './classes/geometry';
import { GeoLocatorService } from './geo-locator.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public locationDetails = new Geometry;
  public userLocation:string;
  
  constructor(private geoLocatorService:GeoLocatorService) {
  }

  setLocation(locationForm){
    this.geoLocatorService.setLocation(this.userLocation);
    this.getLatitudeAndLongitude();
  }

  getLatitudeAndLongitude(){
    this.locationDetails=this.geoLocatorService.getresponse();
  }
}
