using System;

namespace error_handling
{
    class ATMSimulator{
    private double ATMCash = 20000;
    public ATMSimulator()  
     {  
     }

     public Boolean validateUserCard(String card_no, User ATMUser)
     {
         if(card_no != ATMUser.CardNumber)
         {
             return false;
         }
         else
         {
            return true;
         }

     }
    public Boolean isValidateUserPin(int userPin, User ATMUser)
    {
        if(ATMUser.ATMPin == userPin)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

     public double Withdraw(User ATMUser, double withdrawalAmount )
     {
         
         if(withdrawalAmount< ATMUser.AccountBalance)
         {
            ATMUser.AccountBalance = ATMUser.AccountBalance - withdrawalAmount;
            ATMCash -= withdrawalAmount;
        }
        else
        {
             throw new InsufficientBalanceException("Insufficient balance.");
        }
        return withdrawalAmount;
        }
    }
}