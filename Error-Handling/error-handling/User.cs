using System;
namespace error_handling 
{
    class User 
    {
        private string userName;
        private string card_no;  
        private double balance;  
        private int atmPin;
        public User(string name, string card_no, double balance, int pin)  
        {  
           this.userName = name;
           this.card_no = card_no;
           this.balance = balance;
           this.atmPin = pin;
        } 
        public string UserName  
        {  
            get { return userName; }  
            set { userName = value; }  
        } 
        public string CardNumber  
        {  
            get { return card_no; }  
            set { card_no = value; }  
        } 
        public double AccountBalance  
        {  
            get { return balance; }  
            set { balance = value; }  
        }
        public int ATMPin 
        {  
            get { return atmPin; }  
            set { atmPin = value; }  
        } 
    }
}