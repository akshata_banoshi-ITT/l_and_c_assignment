using System;
namespace error_handling 
{
    class Server
    {
        public Boolean serverStatus;
        public Server()
        {
            var random = new Random();
            serverStatus = random.Next(2) == 1;
        }
        public Boolean getServerStatus()
        {
            return serverStatus;
        }
    }
}