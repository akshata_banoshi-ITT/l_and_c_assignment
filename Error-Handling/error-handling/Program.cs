﻿using System;

namespace error_handling
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice, maxAttempts=3, attempt=0, userPin;
            String cardNumber;
            ATMSimulator ATMInstance = new ATMSimulator();
            User ATMUser = new User("Aisha", "ICICxx8690", 15000, 1234);
            Server server = new Server();

            if(server.getServerStatus())
            {

            Console.WriteLine("Welcome");
            Console.WriteLine("Enter the card:");
            cardNumber = Console.ReadLine();
            if(ATMInstance.validateUserCard(cardNumber, ATMUser))
            {
                Console.WriteLine("Enter your pin:");

                while(attempt<maxAttempts)
                {
                    userPin=Convert.ToInt32(Console.ReadLine());
                    if(ATMInstance.isValidateUserPin(userPin, ATMUser))
                    {
                        break;
                    }
                    else
                    {
                        attempt++;
                    }
                }
                if(attempt == maxAttempts)
                {
                    Console.WriteLine("Card Blocked");
                    return;
                }
                Console.WriteLine("1. Withdraw Cash \n 2. Exit");
                choice=Convert.ToInt32(Console.ReadLine());
                switch(choice)
                {
                    case 1: Console.WriteLine("Enter the amount");
                            double withdrawalAmount = 0;
                            try
                            {
                              withdrawalAmount = Convert.ToDouble(Console.ReadLine());  
                            }
                            catch(FormatException exception)
                            {
                                Console.WriteLine("Enter a valid amount");
                            }
                            try
                            {
                                ATMInstance.Withdraw(ATMUser, withdrawalAmount);
                            }catch{
                            }
                            break;
                    case 2: return;

                    }
                }
            }
            else
            {
                Console.WriteLine("Unable to connect to server!!");
            }
        }
    }
}
