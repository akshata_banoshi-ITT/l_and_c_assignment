using System;
namespace error_handling 
{
    class InsufficientBalanceException : Exception
    {
        public InsufficientBalanceException(string errorMessage)
        : base(errorMessage)
        {
            Console.WriteLine(errorMessage);

        }

    }
}
