using System;

namespace AuthenticationSystem{
    public class OTPAuthentication : Authenticate{
        private string mobileNumber="8904711506";
        private string OTP="1234";
        public OTPAuthentication(){
            Console.WriteLine("Authentication with via OTP");
        }
        public override void AuthenticateUser(string key, string value){
            if(key==mobileNumber && value==OTP){
                Console.WriteLine("Logged In successfully!!");
            }
            else{
                Console.WriteLine("Wrong OTP");
            }

        }
    }
}
