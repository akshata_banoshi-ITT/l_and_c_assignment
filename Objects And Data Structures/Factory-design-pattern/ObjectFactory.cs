using System;

namespace AuthenticationSystem {
    public class ObjectFactory{
        private ObjectType _type;
        public Authenticate GetObject(ObjectType type){
             _type=type;
            Authenticate obj=null;
             switch(_type){
                case ObjectType.UserNameAuthentication: obj = new UserNameAuthentication();
                                                        break;
                case ObjectType.OTPAuthentication:obj=new OTPAuthentication();
                                                      break;
                default: obj = null;
                        break;
             }
            return obj;
        }

}
    
    
}
