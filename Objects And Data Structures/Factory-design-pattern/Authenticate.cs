using System;

namespace AuthenticationSystem{

public abstract class Authenticate
{
    public abstract void AuthenticateUser(string key, string value);
}
}