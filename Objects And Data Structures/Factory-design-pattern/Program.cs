﻿using System;

namespace AuthenticationSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            string key, value;
            ObjectFactory objectFactory = new ObjectFactory();
            Authenticate authObject;
            Console.WriteLine("Select the method for Authentication");
            Console.WriteLine("1. Username-password Athentication \n2. OTP Authentication");
            choice = Convert.ToInt32(Console.ReadLine());
            switch(choice){
                case 1: authObject= objectFactory.GetObject(ObjectType.UserNameAuthentication);
                        Console.WriteLine("Enter username:");
                        key=Console.ReadLine();
                        Console.WriteLine("Enter passsword:");
                        value=Console.ReadLine();
                        authObject.AuthenticateUser(key, value);
                        break;
                case 2: authObject = objectFactory.GetObject(ObjectType.OTPAuthentication);
                        Console.WriteLine("Enter mobile number:");
                        key=Console.ReadLine();
                        Console.WriteLine("Enter OTP:");
                        value=Console.ReadLine();
                        authObject.AuthenticateUser(key, value);
                        break;
            }
        }
    }
}
