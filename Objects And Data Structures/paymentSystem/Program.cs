﻿using System;

namespace paymentSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Customer customer = new Customer();
            customer.depositMoney(100);
            PaperBoy boy = new PaperBoy();
            boy.getPayment(customer);
        }
    }
}
