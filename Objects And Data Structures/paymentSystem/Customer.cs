using System;

namespace paymentSystem{
    public  class Customer {    
        private String firstName;    
        private String lastName;    
        private Wallet myWallet;
        public Customer(){
            Console.WriteLine("Customer Created!!");
            myWallet = new Wallet();
        }
        public String getFirstName(){       
            return firstName;
        }
        public String getLastName(){        
            return lastName;    
        }
        public void depositMoney(float money){
            myWallet.addMoney(money);
        }
        public float getPayment(float bill) {        
        if (myWallet != null) {           
            if (myWallet.getTotalMoney() > bill){               
                myWallet.subtractMoney(bill);                
                return bill;            
                }
                else{
                    return 0;
                }
            }
            else{
                return 0;
            }
        }
    }
}