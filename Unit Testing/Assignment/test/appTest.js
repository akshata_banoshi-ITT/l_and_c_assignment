const assert = require('chai').assert;
const app = require('../src/app');

describe('App', function(){

    it('should check for minimum limit of number of test cases', function(){
        let expectedResult = 'Invalid number of test cases';
        let result = app.checkArguementsAndGetResult(0);
        assert.deepEqual(result, expectedResult);
    });

    it('should check for maximim limit of number of test cases', function(){
        let expectedResult = 'Invalid number of test cases';
        let result = app.checkArguementsAndGetResult(1000001);
        assert.deepEqual(result, expectedResult);
    });

    it('should check whether number of test cases and number of inputs are same', function(){
        let inpuValues = [3, 15];
        let expectedResult = 'Number of test cases and number of inputs does not match'
        let result = app.checkArguementsAndGetResult(1, inpuValues);
        assert.equal(result, expectedResult);
    });

    it('should check if any input value is less than 3', function(){
        let inpuValues = [1, 15];
        let expectedResult = 'Input value is out of range'
        let result = app.checkArguementsAndGetResult(2, inpuValues);
        assert.equal(result, expectedResult);
    });

    it('Should return 1,2 as the result for range 3 and 15', function(){
        let inpuValues = [3, 15];
        let expectedResult = [1, 2]
        let result = app.checkArguementsAndGetResult(2, inpuValues);
        assert.deepEqual(result, expectedResult);
    });

    it('Should return 1,2 and 15 as the result for range 3,15 and 100', function(){
        let inpuValues = [3, 15, 100];
        let expectedResult = [1, 2, 15]
        let result = app.checkArguementsAndGetResult(3, inpuValues);
        assert.deepEqual(result, expectedResult);
    });

    it('Should return 1 as number of integers having equal number of divisors for range 3', function(){
        let expectedResult = 1;
        let result = app.calculateNoOfIntegersHavingSameDivisor(3);
        assert.equal(result, expectedResult);
    });

    it('Should return 1 as total number of divisors for 1', function(){
        let expectedResult = 1;
        let result = app.getDivisorOfIndividualNumber(1);
        assert.equal(result, expectedResult);
    });

    it('Should return 3 as total number of divisors for 4', function(){
        let expectedResult = 3;
        let result = app.getDivisorOfIndividualNumber(4);
        assert.equal(result, expectedResult);
    });
})