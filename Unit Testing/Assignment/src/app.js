'use strict';

module.exports = {

  checkArguementsAndGetResult: function(noOfTestCases, arrayOfInputs){

    if(noOfTestCases < 1 || noOfTestCases > 1000000){
      return 'Invalid number of test cases';
    }
    else{
      if(noOfTestCases != arrayOfInputs.length){
        return 'Number of test cases and number of inputs does not match';
      }
      else{
  
        for(let i=0; i<arrayOfInputs.length;i++){
          if(arrayOfInputs[i]<3){
            return 'Input value is out of range';
          }
        }
  
        var result = new Array(noOfTestCases);
        result.fill(0);
        for( let i=0; i<noOfTestCases; i++){
          result[i] = this.calculateNoOfIntegersHavingSameDivisor(arrayOfInputs[i]);
        }
        return result;
      }
    }
  },

  calculateNoOfIntegersHavingSameDivisor: function(range) {
    var high = range + 1;
    var divisors = new Array(high);
    divisors.fill(0);
    var prefixSum = new Array(high);
    prefixSum.fill(0);

    for (let i = 1; i < high; i++) {
      divisors[i] = this.getDivisorOfIndividualNumber(i);
    }
    
    var ans = 0;
    for (let i = 2; i < high; i++)
    {
      if (divisors[i] == divisors[i - 1])
        ans++;
      prefixSum[i] = ans;
    }
    return prefixSum[range];
  },

  getDivisorOfIndividualNumber: function(value){

    var divisors = 0;
    for (let j = 1; j * j <= value; j++) {
                   
        if (value % j == 0) {
          if (j * j == value){
            divisors = divisors + 1;
          }
          else {
            divisors = divisors + 2;
          }
        }
      }
      return divisors;
   }
}